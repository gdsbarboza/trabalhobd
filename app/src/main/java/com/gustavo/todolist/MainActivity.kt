package com.gustavo.todolist

import android.os.Bundle
import android.widget.ArrayAdapter

import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.gustavo.todolist.bd.dao.TarefaDao
import com.gustavo.todolist.entidades.Tarefa
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var db: AppDatabase
    lateinit var tarefaDao: TarefaDao
    lateinit var adapter: ArrayAdapter<Tarefa>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        db = Room.databaseBuilder(applicationContext, AppDatabase::class.java,"tarefa.db").allowMainThreadQueries().build()
        tarefaDao = db.tarefaDao()

        btAdicionar.setOnClickListener{
            val titulo = txtTitulo.text.toString()
            val descricao = txtDescricao.text.toString()

            val tarefa = Tarefa(titulo, descricao, true)

            tarefaDao.inserir(tarefa)

            atualizaLista()
            atualizacampos()
        }
        atualizaLista()

        //listTarefas.setOnLongClickListener(val pessoa = adapter.getItem(position))
    }

    fun atualizaLista(){
        val tarefa = tarefaDao.buscaTodas()
        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, tarefa)
        listTarefas.adapter = adapter
    }

    fun atualizacampos(){
        txtTitulo.setText("")
        txtDescricao.setText("")
    }
}
