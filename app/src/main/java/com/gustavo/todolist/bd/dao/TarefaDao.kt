package com.gustavo.todolist.bd.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.gustavo.todolist.entidades.Tarefa

@Dao
interface TarefaDao{
    @Query("Select * from tarefas")
    fun buscaTodas (): List<Tarefa>

    @Query("select * from tarefas where id = :id LIMIT 1")
    fun buscaTarefa(id: Int): Tarefa

    @Insert
    fun inserir(tarefa: Tarefa)

    @Query("update tarefas set ativo = :status where id = :id")
    fun atualizar(id: Int, status: Boolean): Boolean
}