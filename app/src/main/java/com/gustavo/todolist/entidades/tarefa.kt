package com.gustavo.todolist.entidades

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tarefas")
data class Tarefa(
        var titulo: String,
        var descricao: String,
        var ativo: Boolean
){
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    val getTitulo get() = "$titulo"
    val getDescricao get() = "$descricao"
    val getId get() = "$id"

}