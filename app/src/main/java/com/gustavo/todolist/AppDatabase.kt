package com.gustavo.todolist

import androidx.room.Database
import androidx.room.RoomDatabase
import com.gustavo.todolist.entidades.Tarefa
import com.gustavo.todolist.bd.dao.TarefaDao

@Database(entities = arrayOf(Tarefa::class), version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract  fun  tarefaDao(): TarefaDao
}